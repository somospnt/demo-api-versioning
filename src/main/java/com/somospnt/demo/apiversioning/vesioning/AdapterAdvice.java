package com.somospnt.demo.apiversioning.vesioning;

import static java.lang.Integer.parseInt;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@RestControllerAdvice
public class AdapterAdvice implements ResponseBodyAdvice<Versioned> {

    private static final String PROTOCOL_VERSION_HEADER = "X-Protocol-Version";

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {

        if (methodParameter.getGenericParameterType().getClass().isInstance(Versioned.class)) {
            for (Class<?> interf : ((Class<?>) methodParameter.getGenericParameterType()).getInterfaces()) {
                if (interf.equals(Versioned.class)) {
                    return true;
                }
            }
        }
        
        return false;
    }

    @Override
    public Versioned beforeBodyWrite(Versioned versioned,
            MethodParameter mp,
            MediaType mt, Class<? extends HttpMessageConverter<?>> type,
            ServerHttpRequest serverHttpRequest,
            ServerHttpResponse shr1) {

        String version = serverHttpRequest.getHeaders().getFirst(PROTOCOL_VERSION_HEADER);

        if (version == null) {
            throw new IllegalArgumentException(String.format("Missing '%s' header.", PROTOCOL_VERSION_HEADER));
        }
        
        return versioned.toVersion(parseInt(version));
    }
}
