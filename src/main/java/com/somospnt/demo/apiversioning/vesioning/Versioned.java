package com.somospnt.demo.apiversioning.vesioning;

public interface Versioned {
    Versioned toVersion(int version);
}