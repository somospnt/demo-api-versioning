package com.somospnt.demo.apiversioning.repository;

import com.somospnt.demo.apiversioning.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    
}
