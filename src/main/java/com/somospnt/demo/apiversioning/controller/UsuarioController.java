package com.somospnt.demo.apiversioning.controller;

import com.somospnt.demo.apiversioning.controller.dto.UsuarioDTO;
import com.somospnt.demo.apiversioning.repository.UsuarioRepository;
import com.somospnt.demo.apiversioning.domain.Usuario;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
    
    private final UsuarioRepository usuarioRepository;

    public UsuarioController(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
    
    @GetMapping("/api/usuarios/{id}")
    public UsuarioDTO buscarPorId(@PathVariable("id") long id) {
        Usuario usuario = usuarioRepository.findById(id).orElse(null);
        return new UsuarioDTO(usuario.getNombre(), usuario.getUsername(), usuario.getEmail());
    }

}
