package com.somospnt.demo.apiversioning.controller.dto;

import com.somospnt.demo.apiversioning.vesioning.Versioned;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioDTOv1 implements Versioned {

    private final String nombre;

    @Override
    public Versioned toVersion(int version) {
        return this;
    }

}
