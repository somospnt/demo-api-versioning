package com.somospnt.demo.apiversioning.controller.dto;

import com.somospnt.demo.apiversioning.vesioning.Versioned;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioDTO implements Versioned {

    private final String nombre;
    private final String username;
    private final String email;

    @Override
    public Versioned toVersion(int version) {
        if (version <= 2) {
            return new UsuarioDTOv2(nombre, username).toVersion(version);
        } else {
            return this;
        }
    }

}
