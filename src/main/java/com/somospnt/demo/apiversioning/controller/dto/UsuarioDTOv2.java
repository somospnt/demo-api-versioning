package com.somospnt.demo.apiversioning.controller.dto;

import com.somospnt.demo.apiversioning.vesioning.Versioned;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioDTOv2 implements Versioned {
    
    private final String nombre;
    private final String apellido;
    
    @Override
    public Versioned toVersion(int version) {
        if (version <= 1) {
            return new UsuarioDTOv1(nombre).toVersion(version);
        } else {
            return this;
        }
    }
}
